/*
The MIT License (MIT)

Copyright (c) 2018 David Dix (node-red@dixieworld.co.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
'use strict';
// TODO: Figure out how to enable debug logs just for the yamaha nodes

/**
 * Receiver Config Node
 * @module yamaha-avr
 * @requires module:request
 * @requires module:request-promise-native
 * @requires module:events
 * @requires module:dgram
 */

// Constants:
const DEFAULT_HTTP_TIMEOUT = 5000;

module.exports = function(RED) {
  const requestpn = require('request-promise-native');
  const EventEmitter = require('events');
  const dgram = require('dgram');
  const rcvapi = require('../lib/receiver-api.js');

  let nodeSockets = new Object();
  /**
   * Create a socket and bind to it for listening to UDP events.
   * Multiple instances of the node all share the same socket unless the 
   * port number if different
   * @function
   * @param {string}   id   id of the node
   * @param {integer}  port port number to bind to
   * @return {Object}  socket object
  */
  function createSocket(node, port) {
    if (nodeSockets[port]) {
      if (nodeSockets[port].ids.indexOf(node.id) == -1)
        nodeSockets[port].ids.push(node.id);
    } else {
      // Couldn't find a node using that port, so create one
      let socket = dgram.createSocket('udp4');
      nodeSockets[port] = new Object();
      nodeSockets[port].socket = socket;
      nodeSockets[port].ids = [node.id];

      // Handle any errors that occur on the socket
      socket.on('error', function(err) {
        if (err.code === 'EADDRINUSE') {
          node.warn('The chosen port is already in use on this machine: ' + port);
        } else {
          node.error('Something bad happened: ' + err.stack);
        }
        socket.close();
      });
  
      // Bind it to the port now
      socket.bind(port);
    }
    return nodeSockets[port].socket;
  }

  /**
   * Remove a socket that is used for listening to UDP events.
   * Multiple instances of the node all share the same socket so it is 
   * only removed if the last node to use it
   * @function
   * @param {string}   id   id of the node
   * @param {integer}  port port number to bind to
  */
  function removeSocket(node, port) {
    if (nodeSockets[port]) {
      let idx = nodeSockets[port].ids.indexOf(node.id);
      if (idx > -1) nodeSockets[port].ids.splice(idx,1);
      if (nodeSockets[port].ids.length != 0) {
        nodeSockets[port].socket.close();
        delete nodeSockets[port];
      }
    }
  }

  function YamahaAvrServer(config) {
    RED.nodes.createNode(this,config);

    // Configuration options from the config node setup dialog
    this.name = config.name;
    this.host = config.host;
    this.apiVers = config.apiVers ? config.apiVers : 'v1';
    this.port = config.port ? config.port : 41100;
    // this.log('Host Name is: ' + this.host.toString());
    // node properties

    const node = this;
    node.active = false;
    node.features = null;
    node.deviceInfo = null;
    node.timeoutObj = null;
    node.intervalObj = null;

    // Set the defaults for any HTTP requests made by this node.
    // Set headers to ensure that the receiver will emit UDP events on the 
    // appropiate port to let us know of any status changes
    node.requestpn = requestpn.defaults({
      method: 'GET',
      baseUrl: `http://${node.host}/YamahaExtendedControl/${node.apiVers}/`,
      headers: {
        'User-Agent': 'Request-Promise',
        'X-AppName': 'MusicCast/1.40',
        'X-AppPort': node.port
      },
      timeout: DEFAULT_HTTP_TIMEOUT,
      json: true  // Automatically parses the JSON string in the response
    });

    // Create my own event emitter to tell the other nodes what's going on
    class AVReceiver extends EventEmitter {}
    node.avrEmitter = new AVReceiver();

    // Create a socket to listen for UDP events
    node.udpListener = createSocket(node, node.port);

    node.udpListener.on('listening', function() {
      /**
       * Send request to receiver to get it to emit UDP events.
       * If receiver is offline periodically retry request.
       * If receiver is online resend request every 5 mins to ensure receiver
       * knows someone is listening and continues to emit UDP events
       * @function
       * @return void
      */
      function requestReceiverEvents() {
        node.requestpn({uri: 'system/getDeviceInfo'})
        .then(() => {
          node.intervalObj = setTimeout(requestReceiverEvents, 300000);
          node.debug('Resending the request to keep UDP receiver emitting events');
        })
        .catch(err => {
          node.intervalObj = setTimeout(requestReceiverEvents, 15000);
          node.debug('Retry until reciever comes back online: ' + err.message);
        });
      }
      requestReceiverEvents();
      node.avrEmitter.emit('listening');
    });
    
    node.udpListener.on('message', function(udpMsg) {
      const event = JSON.parse(udpMsg);
      // Verify it's a Yamaha AVR event and the event is for this node before passing it on
      if (event.device_id !== undefined
            && node.deviceInfo
            && event.device_id === node.deviceInfo.device_id) {
        node.debug('AVR event: ' + udpMsg.toString());
        node.avrEmitter.emit('message', event);              
      }
    });

    /**
     * Get the features and device information from the receiver
     * Keep retrying if there is no response and set the node 'active' state
     * only when we have received a response.
     * @function
     * @return void
    */
    function initFeaturesAndInformation() {
      node.requestpn({uri: 'system/getFeatures'})
      .then(function (response) {
        node.features = response;
        // options.uri = 'system/getDeviceInfo';
        return node.requestpn({uri: 'system/getDeviceInfo'});
      })
      .then(function(response) {
        node.deviceInfo = response;
        // Once we have the info and features the node is can be made active
        node.active = true;
      })
      .catch(function(err) {
        if (err.response) {
          // We should never get here because all receivers support these API calls
          node.debug('request status code error: ' + err.response.status);
        } else if (err.error.connect) {
          node.avrEmitter.emit('retrying');
          node.debug('Failed to get AVR features. Retry in 30s: ' + err.message);
          node.timeoutObj = setTimeout(initFeaturesAndInformation, 30000);
        } else {
          node.error('initNode: ' + err.message);
        }
      });
    }
    initFeaturesAndInformation();

    // ---------------------- node Methods ---------------------


    // ---------------------- node Event Handling --------------

    node.on('close', function(removed, done) {
      if (node.timeoutObj) clearTimeout(node.timeoutObj);
      if (node.intervalObj) clearTimeout(node.intervalObj);
      // Close the UDP socket
      removeSocket(node.id, node.port);
      delete node.requestpn;
      delete node.avrEmitter;
      if (removed) {
        // This node has been deleted
        node.debug('close called with remove flag');
      } else {
        // This node is being restarted
        node.debug('close called without remove flag');
      }
      // done() must be called within 15 seconds of the close() function call
      done();
    });

    // --------------------- Functions -------------------
  }


  // ------------ Node HTTP Server ------------------

  /**
   * Get receiver device information directly form the device
   * @function
   * @param {string} host - IP Address of the receiver (config) node
   * @return {object} The deviceInfo object as freturned from the receiver
   */
  // eslint-disable-next-line no-unused-vars
  RED.httpAdmin.get('/yamaha-avr/deviceInfo', (req, res, next) => {
    if (!req.query.host) {
      return res.status(400).send('Missing host address');
    }
    const url = `http://${req.query.host}/YamahaExtendedControl/v1/system/getDeviceInfo`;

    requestpn.get({uri: url, timeout: 2000})
    .then(response => {
      return res.end(response);
    })
    .catch(err => {
      return res.status(408).send(err.message);
    });
  });

  /**
   * Get the zones directly from the the receiver using its IP address
   * @function
   * @param {string} host - IP address of the receiver (config) node
   * @return {Array} An array of 'zone:friendly name' pairs
   */
  // eslint-disable-next-line no-unused-vars
  RED.httpAdmin.get('/yamaha-avr/zones', (req, res, next) => {
    if (!req.query.host) {
      return res.status(400).send('Missing host address');
    }
    const url = `http://${req.query.host}/YamahaExtendedControl/v1/system/getFeatures`;

    requestpn.get({uri: url, json: true, timeout: 2000})
    .then(response => {
      let zones = new Array();
      const features = response;
      features.zone.forEach(zone => {
        zones.push(rcvapi.getZoneNameObj(zone.id));
      });
      return res.end(JSON.stringify(zones));
    })
    .catch(err => {
      return res.status(408).send(err.message);
    });
  });

  /**
   * Get a list of controllers for a specified controller type
   * @function
   * @param {string} type - Controller type e.g. status|control.
   * @return {Array} An array of 'controller:friendly name' pairs
   */
  // eslint-disable-next-line no-unused-vars
  RED.httpAdmin.get('/yamaha-avr/controllers', function(req, res, next){
    // RED.log.info('Controllers: ' + JSON.stringify(controllers));
    res.end(JSON.stringify(rcvapi.controllers()));
  });

  /**
   * Get a list of commands supported by the supplied controller
   * @function
   * @param {string} controller - Controller e.g. system|main|tuner.
   * @param {string} type       - type e.g. status|control.
   * @return {Array} An array of 'command:friendly name' pairs
   */
  // eslint-disable-next-line no-unused-vars
  RED.httpAdmin.get('/yamaha-avr/commands', function(req, res, next){
    if (!req.query.controller) {
      return res.status(400).send('Missing controller');
    }
    const controller = req.query.controller;
    const type = req.query.type;
    const commands = rcvapi.commands(controller, type);
    return res.end(JSON.stringify(commands));
  });


  RED.nodes.registerType('yamaha-avr',YamahaAvrServer);
};
