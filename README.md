# Node-RED nodes for Yamaha Audio/Video Receiver (AVR)
This package contains nodes to easily integrate and control YAMAHA™ audio/video receivers from Node-Red (e.g. AVRs like the model Yamaha RX-A3070).


## Installation
Install using Manage Palette menu item from inside Node-RED.

## Usage
Three new nodes will appear in the category 'Home Cinema' in your Node-Red palette: Yamaha AVR Listener, Yamaha AVR Status, Yamaha AVR Control. They can be used to monitor and control your Yamaha AV equipment.

![nodes.png](./doc/nodes.png)

#### AVR Yamaha Listener
The Listener node monitors UDP events emitted from the receiver and outputs a `msg.payload` when certain events occur. The UDP events monitored are the same as those used by Yamaha's Multicast music entertainment system. The node can be configured to output the events unprocessed or to process and output specific events. All events are output encoded in JSON format. The `msg.topic` property will contain the zone assciated withthe event e.g. `{topic: "zone2"}`

The Listener only outputs the events for the receiver and zone specified. It will filter out all other events and discard them. The receiver and zone can be selected during configuration of the node.

The list of events are:

 * Power: Outputs `{power: "on"}` or `{power: "standby"}` when the power status of the reciever changes.
 * AV Input: Outputs `{input: "av1"}` or `{input: "audio1"}` when the receiver AV input selection changes.
 * Volume: Outputs `{volume: 102}` or `{volume: 95}` when the receiver volume changes.
 * Unprocessed: Outputs the event sent from the reciever unprocessed e.g. An "On" event may be output as `{"main":{"power":"on"},"device_id":"00A0AEFB28B5"}`. For unprocessed events the `msg.topic` property will contain `"unprocessed"`.


Note: Events may be combined into a single `msg.payload` e.g. `{"power":"on","volume":103}`.

#### Yamaha AVR Status
The Status node retrieves status information from the receiver. The node presents all of the currently defined status API calls grouped by function e.g. System information, Zone information, Tuner information etc. After selecting a function you are presented with a list of status requests supported by the function.

The Receiver, Function and Request are configurable options:
* Receiver: Select a receiver as specified in the Config node
* Function: Select a function you require the status from e.g. `System` or `Tuner`. The functions are retrieved from the receiver by the Config node and may be different depending the make & model of the receiver. If the `Function` is set to `msg.payload` then the status request is taken from the input `msg.payload` e.g. setting the `msg.payload` to `{"system/getFeatures"}` will retrieve the receiver features from the system function.
* Request: Select a request to be sent to the receiver. The list of requests changes based on the `Function` selected.

Note: If blank, the `Name` will be set to the request selected. Setting the `Name` to anything else will override this feature.

The node will output a message with the `msg.payload` property set to the response from the receiver in JSON format and the `msg.url` property set to the request URL.


#### Yamaha AVR Control
The Control node sends commands to the receiver. The node provides a means to send a basic set of most frequently used commands, but also supports sending any Yamaha™ Extended Control API command (see node Help for further details).

The input message should contain a `msg.payload` property containing the command, or combination of commands to tbe sent to the receiver and a `msg.topic` property containing the zone to send the command to (if no `msg.topic` is set then the zone will be sent to `main`).

The basic commands consist of:
* Power Control: Control a zones power state e.g. Input `msg.payload` contains `{power: "on"}`
* Volume Control: Control a zone's volume e.g. Input `msg.payload` contains `{volume: 100}` or `{volume: "up"}`
* Mute Control: Control a zone's mute state e.g. Input `msg.payload` contains `{mute: "on"}`
* Sleep Control: Control when a zone should automatically switch off e.g. Input `msg.payload` contains `{sleep: 60}`
* Input Control: Control a zone's AV input e.g. Input `msg.payload` contains `{input: "av1"}`
* Sound Program Control: Control a zone's Sound Program e.g. Input `msg.payload` contains `{soundProgram: "2ch_stereo"}`

An input message can contain multiple requests in the same payload, but only one request for each command type e.g. Set input `msg.payload` to `{power: "on", volume: 100, input: "av1", soundProgram: "2ch_stereo"}`.

The output of a Control request consists of a message containing the following properties:
* `msg.payload`: The response from the receiver in JSON format.
* `msg.request`: The request sent to the receiver in JSON format.
* `msg.topic`: The zone the request was sent to.

You can get details of advanced commands supported by the API and more details on individual commands by sending a `help` payload to the node e.g. `{help: "all"}`, `{help: "getFeatures"}`


Refer to the node Help page for more information.

#### Config Node
The Config node holds the information about which receiver to listen to or send the requests to.
Use this node to set the IP address of the receiver. Additionally the default `API verison` and UDP listening `port` can be overridden. The defaults should work for the majority of receivers supporting the Yamaha Extended Control API e.g. 'v1' & 41100.

Once configured the node will attempt to connect to the reciever and retrieve device information etc. The Status, Control or Listener nodes will show the connection status.

### Additional Information
Hint: To remotely power on the AVR, the network standby has to be enabled in the internal settings of the AVR.

### Example 1:
The following flow shows same basic control of the AVR.

![example1.png](./doc/example1.png)

    [{"id":"8587392b.f1502","type":"tab","label":"Flow 1","disabled":false,"info":""},{"id":"6289a871.cb1da8","type":"yamaha-avr-listener","z":"8587392b.f1502","receiver":"e873eb4e.8a72e","name":"RX-A3070: main","zone":"main","unprocessed":false,"power":true,"avinput":false,"volume":false,"x":420,"y":100,"wires":[["3863ced.15c0732"]]},{"id":"209dd32a.e7c7d4","type":"yamaha-avr-status","z":"8587392b.f1502","receiver":"e873eb4e.8a72e","name":"Get Device Information","controller":"system","command":"getDeviceInfo","x":390,"y":200,"wires":[["3863ced.15c0732"]]},{"id":"8a2374be.72797","type":"yamaha-avr-status","z":"8587392b.f1502","receiver":"e873eb4e.8a72e","name":"Get Sound Progams","controller":"main","command":"getSoundProgramList","x":400,"y":260,"wires":[["3863ced.15c0732"]]},{"id":"cdaaa64b.8052b8","type":"yamaha-avr-control","z":"8587392b.f1502","receiver":"e873eb4e.8a72e","name":"","x":400,"y":340,"wires":[["3863ced.15c0732"]]},{"id":"3863ced.15c0732","type":"debug","z":"8587392b.f1502","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"false","x":650,"y":220,"wires":[]},{"id":"c872db22.eb87f8","type":"inject","z":"8587392b.f1502","name":"Power On Main","topic":"main","payload":"{\"power\": \"on\", \"volume\": 100}","payloadType":"json","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":120,"y":340,"wires":[["cdaaa64b.8052b8"]]},{"id":"2ab197d9.e39cd8","type":"inject","z":"8587392b.f1502","name":"Stereo Sound Program","topic":"main","payload":"{\"soundProgram\": \"2ch_stereo\"}","payloadType":"json","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":140,"y":400,"wires":[["cdaaa64b.8052b8"]]},{"id":"f8dc423a.3920c","type":"inject","z":"8587392b.f1502","name":"","topic":"","payload":"true","payloadType":"bool","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":150,"y":200,"wires":[["209dd32a.e7c7d4"]]},{"id":"f55b64ec.520478","type":"inject","z":"8587392b.f1502","name":"","topic":"","payload":"true","payloadType":"bool","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":150,"y":260,"wires":[["8a2374be.72797"]]},{"id":"e873eb4e.8a72e","type":"yamaha-avr","z":"","name":"RX-A3070","host":"192.168.1.16","apiVers":"v1","port":""}]


### Example 2:
The following example shows how simple it is to setup a single inject node to configure your home cinema to watch Netflix. The inject node has a number of commands in its output payload to: Turn on the receiver, select input "av1" (the Netflix source), set the initial volume to 100, set the sound program to "surr_decode" and activate the auto-off sleep timer (because it's late night viewing and I might fall asleep).


![example2.png](./doc/example2.png)

    [{"id":"8587392b.f1502","type":"tab","label":"Flow 1","disabled":false,"info":""},{"id":"cdaaa64b.8052b8","type":"yamaha-avr-control","z":"8587392b.f1502","receiver":"e873eb4e.8a72e","name":"","x":400,"y":340,"wires":[["3863ced.15c0732"]]},{"id":"3863ced.15c0732","type":"debug","z":"8587392b.f1502","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"false","x":650,"y":340,"wires":[]},{"id":"c872db22.eb87f8","type":"inject","z":"8587392b.f1502","name":"Watch Netflix","topic":"main","payload":"{\"power\":\"on\",\"volume\":100,\"input\":\"av1\",\"soundProgram\":\"surr_decoder\",\"sleep\":90}","payloadType":"json","repeat":"","crontab":"","once":false,"onceDelay":0.1,"x":150,"y":340,"wires":[["cdaaa64b.8052b8"]]},{"id":"e873eb4e.8a72e","type":"yamaha-avr","z":"","name":"RX-A3070","host":"192.168.1.16","apiVers":"v1","port":""}]


### Example 3:
The following example shows how the Listener node can be used to turn on the Philips Hue TV side lights when the AV receiver turns on, but only if it's dark enough to be worthwhile.


![example3.png](./doc/example3.png)

    [{"id":"2fe54f6e.e2dbb8","type":"tab","label":"Lounge","disabled":false,"info":"Control of the TV side lights.\n\nIf it is nighttime then the TV sidelights will\ncome on when the AV Receiver is turned on."},{"id":"8912f3d6.c0e3d8","type":"yamaha-avr-listener","z":"2fe54f6e.e2dbb8","receiver":"e873eb4e.8a72e","name":"RX-A3070: main","zone":"main","unprocessed":false,"power":true,"avinput":false,"volume":false,"x":120,"y":60,"wires":[["35d2462e.54346a"]]},{"id":"8e46115.469b67","type":"hue-group","z":"2fe54f6e.e2dbb8","name":"Lounge","bridge":"b24c30d6.d15128","groupid":"1","x":520,"y":80,"wires":[["e47700e5.226ed8"]]},{"id":"fb5cf308.6e1f4","type":"sunrise","z":"2fe54f6e.e2dbb8","name":"Daytime","lat":"51.311324","lon":"-0.617983","start":"sunrise","end":"nauticalDusk","x":140,"y":120,"wires":[["35d2462e.54346a"],[]]},{"id":"e47700e5.226ed8","type":"debug","z":"2fe54f6e.e2dbb8","name":"","active":true,"tosidebar":true,"console":false,"tostatus":false,"complete":"false","x":690,"y":80,"wires":[]},{"id":"35d2462e.54346a","type":"function","z":"2fe54f6e.e2dbb8","name":"Logic","func":"/*\nTurn on if:\n  Night time AND Receiver turns on AND TV Lights are off\n\nTurn off if:\n  (Receiver turns off OR Night time ends) AND TV Lights are on\n*/\n\nlet bNight = context.get('bNight') || false;\nlet bAmpOn = context.get('bAmpOn') || false;\nlet bLightsOn = context.get('bLightsOn') || false;\n\nlet outMsg = null;\n\nif (msg.topic === 'sun') {\n    // Set bNight based on output of SunCalc\n    bNight = msg.payload == 1 ? false : true;\n    context.set('bNight', bNight);\n} else if (msg.payload.power) {\n    // Set bAmpOn based on power event output\n    bAmpOn = msg.payload.power === 'on' ? true : false;\n    context.set('bAmpOn', bAmpOn);\n}\n\n\nif (bNight && bAmpOn && !bLightsOn) {\n    // Its night time, the Amp is on, but the lights aren't, so turn them on\n    context.set('bLightsOn', true);\n    outMsg = {payload: {\"on\": true}};\n} else if ((!bNight || !bAmpOn) && bLightsOn) {\n    // It's day time and the lights are on, so turn them off\n    context.set('bLightsOn', false);\n    outMsg = {payload: {\"on\": false}};\n}\n\nconst message = `night:${bNight}, amp:${bAmpOn}, lights:${bLightsOn}`\n//const message = bNight ? 'night' : 'day' + ': ' + bAmpOn ? 'amp on' : 'amp off' + ': ' + bLightsOn ? 'lights on' : 'lights off';\n\nnode.status({fill:\"green\",shape:\"dot\",text:message});\nreturn outMsg;","outputs":1,"noerr":0,"x":290,"y":80,"wires":[["8e46115.469b67"]]},{"id":"e873eb4e.8a72e","type":"yamaha-avr","z":"","name":"RX-A3070","host":"192.168.1.16","apiVers":"v1","port":""},{"id":"b24c30d6.d15128","type":"hue-bridge","z":"","name":"MyHue","bridge":"192.168.1.110","key":"put your key here","interval":"3000"}]


## Open Topics
- The Config node does not auto-discover receivers and currently requires an IP address. If you are using DHCP for your receivers IP address then I suggest allocating a static address for the receiver until the auto-discovery feature is added.
- Not all receivers support all the functions and commands implemented in the Status & Control nodes. Send `system/getFeatures` and `main/getFuncStatus` status requests to get details of what your receiver supports.
- The Listener does not yet monitor sound program changes or changes to mute.


## List of verified devices
The nodes have been successfully tested with the following devices:
- RX-A3070
- RX-A3060
- RX-A2070


## History
- 2018-Mar-13: 0.1.0 - Initial version

## Credits
- David Dix (node-red@dixieworld.co.uk)

## Acknowledgements
This package was inspired by the original node-red-contrib-avr-yamaha package from Sebastian Krauskopf [https://github.com/krauskopf/node-red-contrib-avr-yamaha.git](https://github.com/krauskopf/node-red-contrib-avr-yamaha.git). I used this as a basis to learn more about node-red and how to create my own nodes. Thanks Sebastian!


## Trademarks
- "YAMAHA" is a registered trademark of Yamaha Corporation.

## License
The MIT License (MIT)

Copyright (c) 2018 David Dix (node-red@dixieworld.co.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
