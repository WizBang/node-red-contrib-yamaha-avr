/*
The MIT License (MIT)

Copyright (c) 2018 David Dix (node-red@dixieworld.co.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
'use strict';

/**
 * Receiver Listener Node
 * @module yamaha-avr-listener
 */
// TODO: Add support for monitoring "mute"
// TODO: Add support for monitoring sound program changes

module.exports = function(RED) {

  function YamahaAvrNodeListener(config) {
    RED.nodes.createNode(this,config);
    const node = this;
    node.status({fill:'red', shape:'ring', text:'initialising'});
    // node.log('Node opening...');

    // Grab my config settings
    node.name = config.name;
    node.eventList = {};
    node.eventList.zone = config.zone;
    node.eventList.unprocessed = config.unprocessed;
    node.eventList.power = config.power;
    node.eventList.volume = config.volume;
    node.eventList.input = config.avinput;


    // Retrieve the YamahaAvrServer config node
    const receiver = RED.nodes.getNode(config.receiver);
    node.receiver = receiver;

    // If we have no receiver then there's no point in carrying on
    if (!receiver) {
      // Config node does not exist
      this.error(RED._('yamaha-avr.errors.missing-config'));
      return;
    }

    // Wait until the receiver is active and then initial the node
    function waitForReceiverConnected() {
      if (receiver && receiver.active) {
        node.status({fill:'green', shape:'dot', text:'connected'});
        // Initialise the supported zones
        node.zones = {};
        for (let name of receiver.features.zone) {
          let zone = {
            enabled: true,
            power: null,
            volume: null,
            input: null
          };
          node.zones[name.id] = zone;
        }
      } else {
        setTimeout(() => { waitForReceiverConnected(); }, 5000);
      }
    }
    waitForReceiverConnected();

    // ---------------------- Event Handling --------------

    receiver.avrEmitter.on('retrying', () => {
      node.trace('Got a retrying event');
      node.status({fill:'yellow', shape:'ring', text:'connecting...'});
    });

    receiver.avrEmitter.on('listening', () => {
      node.trace('Got a listening event');
      node.status({fill:'green', shape:'dot', text:'listening'});
    });

    receiver.avrEmitter.on('message', (event) => {
      node.trace('Got a new AVR event');
      // Verify the message is for this node
      if (node.receiver.deviceInfo.device_id !== event.device_id) return;

      // send the unprocessed event if configured this way
      if (node.eventList.unprocessed) {
        let msg = {topic: 'unprocessed', payload: event};
        node.send(msg);
      } else {
        // Loop thru all zones in the event and send messages if changes
        const zones = node.zones;
        for (let eventZone in event) {
          // Only process zones I know about and are enabled for events
          if (eventZone != node.eventList.zone) continue;
          if (!(eventZone in zones)) continue;
          const zone = zones[eventZone];
          if (!zone.enabled) continue;

          const eventValues = event[eventZone];
          checkForChange(zone, eventValues, function(changes) {
            // construct a message to send
            // One message per zone enabled
            //  msg.topic - Zone name
            //  msg.payload - zone changes
            let msg = {topic: eventZone, payload: changes};
            node.send(msg);
          });
        }
      }
    });

    node.on('close', (removed, done) => {
      if (node.avrEmitter) node.avrEmitter.removeAllListeners();
      if (removed) {
        // This node has been deleted
        node.debug('close called with remove flag');
      } else {
        // This node is being restarted
        node.debug('close called without remove flag');
      }
      // done() must be called within 15 seconds of the close() function call
      done();
    });

    // --------------------- Functions -------------------

    // Check event for changes
    // @zone - zone object to compare against
    // @eventValues - the values from the event we are processing
    // @changes - function to call if changes are present
    function checkForChange(zone, eventValues, changes) {
      let events = {};

      // iterate thru values checking for changes
      for (let val in eventValues) {
        if (node.eventList[val] && zone[val] != eventValues[val]) {
          // Build list of changes & save current values
          events[val] = eventValues[val];
          zone[val] = eventValues[val];
        }
      }
      // Only call callback if there are changes to send
      if (Object.keys(events).length > 0) changes(events);
    }
  }
  
  RED.nodes.registerType('yamaha-avr-listener',YamahaAvrNodeListener);
};
