/*
The MIT License (MIT)

Copyright (c) 2018 David Dix (node-red@dixieworld.co.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
'use strict';

/**
 * Receiver Control Node
 * @module yamaha-avr-control
 */

module.exports = function(RED) {
  const rcvapi = require('../lib/receiver-api.js');

  function YamahaAvrNodeControl(config) {
    RED.nodes.createNode(this,config);
    const node = this;

    // Retrieve the YamahaAvrServer config node
    node.config = RED.nodes.getNode(config.receiver);

    if (node.config) {
      // Get a handle for the receiver
      const receiver = node.config;

      // input:
      // Process the control request by sending message to receiver
      node.on('input', function(msg) {
        const payload = msg.payload;
        let reqConfig = {};
        const zone = msg.topic ? msg.topic : 'main';
        const requests = Object.keys(msg.payload);
        requests.forEach(req => {
          let response = {};
          switch(req) {
            case 'help':
              if (payload.help === 'all') {
                // Send back a list of all commands
                response = rcvapi.getAllCommandDefintions('control');
              } else {
                // Send back a defintion of the command
                response = rcvapi.getCommandDefinition(payload.help);            
              }
              msg.payload = response;
              node.send(msg);
              return;
            case 'command': {
              // TODO: Handle case where the command sent does not exist in API object
              const rcvCmd = rcvapi.getCommandDefinition(payload.command.command);
              reqConfig.method = rcvCmd ? rcvCmd.method : 'GET';
              reqConfig.uri = `${payload.command.controller}/${payload.command.command}`;
              // GET or POST
              if (reqConfig.method === 'GET') {
                reqConfig.qs = payload.command.params;                
              } else {
                reqConfig.body = payload.command.params;
              }
              break;
            }
            case 'power':
              reqConfig.method = 'GET';
              reqConfig.uri = `${zone}/setPower`;
              reqConfig.qs = {power:payload.power};
              break;
            case 'mute':
              reqConfig.method = 'GET';
              reqConfig.uri = `${zone}/setMute`;
              reqConfig.qs = payload.mute === 'on' ? {enable:true} : {enable:false};
              break;
            case 'volume':
              reqConfig.method = 'GET';
              reqConfig.uri = `${zone}/setVolume`;
              reqConfig.qs = {volume:payload.volume, step:payload.step};
              break;
            case 'sleep':
              reqConfig.method = 'GET';
              reqConfig.uri = `${zone}/setSleep`;
              reqConfig.qs = {sleep:payload.sleep};
              break;
            case 'input':
              reqConfig.method = 'GET';
              reqConfig.uri = `${zone}/setInput`;
              reqConfig.qs = {input:payload.input};
              break;
            case 'soundProgram':
              reqConfig.method = 'GET';
              reqConfig.uri = `${zone}/setSoundProgram`;
              reqConfig.qs = {program:payload.soundProgram};
              break;
            default:
              msg.payload = `Unknown command ${req}`;
              node.send(msg);
              return;
          }
          // Send request to the receiver
          msg.request = reqConfig;
          receiver.requestpn(reqConfig)
          .then (response => {
            msg.payload = response;
            msg.payload.response_text = rcvapi.textResponse[response.response_code];
            node.send(msg);       
          })
          .catch(err => {
            msg.payload = err.message;
            node.send(msg);       
          });
        });
      });

      node.on('close', function(removed, done) {
        if (removed) {
          // This node has been deleted
          node.log('close called with remove flag');
        } else {
          // This node is being restarted
          node.log('close called without remove flag');
        }
        // done() must be called within 15 seconds of the close() function call
        done();
      });
    } else {
      // Config node does not exist
      this.error(RED._('yamaha-avr.errors.missing-config'));
    }
  }

  RED.nodes.registerType('yamaha-avr-control',YamahaAvrNodeControl);
};
