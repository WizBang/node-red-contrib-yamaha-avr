/*
The MIT License (MIT)

Copyright (c) 2018 David Dix (node-red@dixieworld.co.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
'use strict';

/**
 * Receiver Status Node
 * @module yamaha-avr-status
 */

module.exports = function(RED) {
  const rcvapi = require('../lib/receiver-api.js');
  
  function YamahaAvrNodeStatus(config) {
    RED.nodes.createNode(this,config);
    this.controller = config.controller;
    this.command = config.command;
    this.parameters = config.parameters;
    const node = this;

    // Retrieve the YamahaAvrServer config node
    node.config = RED.nodes.getNode(config.receiver);

    // Use the settings from the receiver config node
    if (node.config) {
      // Get a handle for the receiver
      const receiver = node.config;

      // input:
      // Process the status request by sending message to receiver
      node.on('input', function(msg) {
        // Create a request and send it to the reciever
        let url = msg.payload;
        const params = node.parameters ? JSON.parse(node.parameters) : {};
        if (node.controller && node.controller != 'msg.payload') {
          url = node.controller + '/' + node.command;
        }
        msg.url = url;
        receiver.requestpn({uri: url, qs: params})
        .then(response => {
          msg.payload = response;
          msg.payload.response_text = rcvapi.textResponse[response.response_code];
          node.send(msg);       
        })
        .catch(err => {
          msg.payload = err.message;
          node.send(msg);       
        });
      });

      node.on('close', function(removed, done) {
        if (removed) {
          // This node has been deleted
          node.debug('close called with remove flag');
        } else {
          // This node is being restarted
          node.debug('close called without remove flag');
        }
        // done() must be called within 15 seconds of the close() function call
        done();
      });
    } else {
      // Config node does not exist
      this.error(RED._('yamaha-avr.errors.missing-config'));
    }
  }

  RED.nodes.registerType('yamaha-avr-status',YamahaAvrNodeStatus);
};
