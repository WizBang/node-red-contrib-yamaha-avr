/*
The MIT License (MIT)

Copyright (c) 2018 David Dix (node-red@dixieworld.co.uk)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the 'Software'), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
'use strict';

/**
 * Receiver API module
 * @module ../lib/receiver-api
 */

//
// The currently known commands supported by Yamaha Receivers
// Accurate as of February 2018 and based off of:
// Yamaha Extended Control API Specification (Basic) v1.10
// available from here: https://jayvee.com.au/downloads/commands/yamaha/YXC_API_Spec_Basic.pdf
//
module.exports = {
  _zones: [
    {value: 'system', name: 'System'},
    {value: 'main', name: 'Main_Zone'},
    {value: 'zone2', name: 'Zone 2'},
    {value: 'zone3', name: 'Zone 3'},
    {value: 'zone4', name: 'Zone 4'},
    {value: 'tuner', name: 'Tuner'},
    {value: 'netusb', name: 'Network/USB'},
    {value: 'cd', name: 'CD'},
    {value: 'clock', name: 'Clock'}
  ],
  /**
   * Provide a friendly name object for a receiver zone
   * @function
   * @return {Object} Zone name object e.g. {value: 'zone2', name: 'Zone 2'}
   */
  getZoneNameObj: function(zone) {
    return this._zones.filter(idx => idx.value === zone)[0];
  },

  /**
   * Provide a list of controllers supported by the receivers
   * @function
   * @return {Array} Array of {value: 'a', name: 'b'} pairs
   */
  controllers: function() {
    return this._zones;
  },

  /**
   * Provide a list of commands available for a specified controller
   * @param  {string} controller Controller required e.g. 'main', 'system'
   * @param  {string} type       Type required e.g. 'status', 'control'
   * @return {Array} List of commands
   */
  commands: function(controller, type) {
    let result = new Array();

    // Search for all the commands in the specified controller and return them
    const cmdObj = this._avrCommands;
    const commands = Object.keys(cmdObj).filter(cmd => cmdObj[cmd].controllers.includes(controller));
    commands.forEach(cmd => {
      if (cmdObj[cmd].type === type)
        result.push({value: cmd, name: cmdObj[cmd].name});
    });
    return result;
  },

  /**
   * Provide the definition of the requested command
   * @function
   * @param  {string} cmd command required e.g. 'getFeatures', 'setNameText'
   * @return {Object} Definition of command
   */
  getCommandDefinition: function(cmd) {
    return this._avrCommands[cmd];
  },

  /**
   * Provide the definition of all the commands of a certain type
   * @function
   * @param  {string} type type required e.g. 'control', 'status'
   * @return {Object} All requested command definitions
   */
  getAllCommandDefintions: function(type) {
    let result = {};
    const cmdObj = this._avrCommands;
    // Get a list of commands meeting the requested type
    const commands = Object.keys(cmdObj).filter(cmd => cmdObj[cmd].type === type);
    commands.forEach(cmd => {
      result[cmd] = cmdObj[cmd];
    });
    return result;
  },
  /**
   * An array of the response codes we can expect from the receiver and
   * the corresponding text string
   * @type {object}
   */
  textResponse: {
    0: 'Success',
    1: 'Initializing',
    2: 'Internal Error',
    3: 'Invalid Request (A method did not exist, a method wasn’t appropriate etc.)',
    4: 'Invalid Parameter (Out of range, invalid characters etc.)',
    5: 'Guarded (Unable to setup in current status etc.)',
    6: 'Time Out',
    99: 'Firmware Updating',
    100: 'Access Error',
    101: 'Other Errors',
    102: 'Wrong User Name',
    103: 'Wrong Password',
    104: 'Account Expired',
    105: 'Account Disconnected/Gone Off/Shut Down',
    106: 'Account Number Reached to the Limit',
    107: 'Server Maintenance',
    108: 'Invalid Account',
    109: 'License Error',
    110: 'Read Only Mode',
    111: 'Max Stations',
    112: 'Access Denied'
  },

  /**
   * A list of commands available for the Yamaha range of AV Receivers
   * @namespace
   * @property {object}  method               - The API method
   * @property {string}  method.name          - The method friendly name
   * @property {string}  method.apiVersion       - The API apiVersion
   * @property {string}  method.method        - The HTTP method
   * @property {string}  method.type          - The type of method; status or control
   * @property {Array}   method.controllers   - The controllers that support the method
   * @property {object}  method.params    - The params required for the method
   */
  _avrCommands: {
    // System Commands
    getDeviceInfo: {
      name: 'Get Device Information',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    getFeatures: {
      name: 'Get Features',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    getNetworkStatus: {
      name: 'Get Network Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    setWiredLan: {
      name: 'Set Wired LAN',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          dhcp: {type: 'string', value: 'any', required: false},
          ip_address: {type: 'string', value: 'any', required: false},
          subnet_mask: {type: 'string', value: 'any', required: false},
          default_gateway: {type: 'string', value: 'any', required: false},
          dns_server_1: {type: 'string', value: 'any', required: false},
          dns_server_2: {type: 'string', value: 'any', required: false}
        }
      }
    },
    setWirelessLan: {
      name: 'Set Wireless LAN',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          ssid: {type: 'string', value: 'any', required: false},
          type: {type: 'string', value: ['none', 'wep', 'wpa2-psk(aes)', 'mixed_mode'], required: false},
          key: {type: 'string', value: 'any', required: false},
          dhcp: {type: 'string', value: 'any', required: false},
          ip_address: {type: 'string', value: 'any', required: false},
          subnet_mask: {type: 'string', value: 'any', required: false},
          default_gateway: {type: 'string', value: 'any', required: false},
          dns_server_1: {type: 'string', value: 'any', required: false},
          dns_server_2: {type: 'string', value: 'any', required: false}
        }
      }
    },
    setWirelessDirect: {
      name: 'Set Wireless Direct',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          type: {type: 'string', value: ['none', 'wpa2-psk(aes)'], required: false},
          key: {type: 'string', value: 'any', required: false}
        }
      }
    },
    setIpSettings: {
      name: 'Set IP Address Settings',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          dhcp: {type: 'string', value: 'any', required: false},
          ip_address: {type: 'string', value: 'any', required: false},
          subnet_mask: {type: 'string', value: 'any', required: false},
          default_gateway: {type: 'string', value: 'any', required: false},
          dns_server_1: {type: 'string', value: 'any', required: false},
          dns_server_2: {type: 'string', value: 'any', required: false}
        }
      }
    },
    setNetworkName: {
      name: 'Set Network Name',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          name: {type: 'string', value: 'any', required: true}
        }
      }
    },
    setAirPlayPin: {
      name: 'Set Airplay PIN',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          pin: {type: 'string', value: 'any', required: true}
        }
      }
    },
    getMacAddressFilter: {
      name: 'Get MAC Address Filter',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    setMacAddressFilter: {
      name: 'Set MAC Address filter',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          filter: {type: 'boolean', value: [true, false], required: false},
          address_1: {type: 'string', value: 'any', required: false},
          address_2: {type: 'string', value: 'any', required: false},
          address_3: {type: 'string', value: 'any', required: false},
          address_4: {type: 'string', value: 'any', required: false},
          address_5: {type: 'string', value: 'any', required: false},
          address_6: {type: 'string', value: 'any', required: false},
          address_7: {type: 'string', value: 'any', required: false},
          address_8: {type: 'string', value: 'any', required: false},
          address_9: {type: 'string', value: 'any', required: false},
          address_10: {type: 'string', value: 'any', required: false},
        }
      }
    },
    getNetworkStandby: {
      name: 'Get Network Standby',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    setNetworkStandby: {
      name: 'Set Network Standby',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          standby: {type: 'string', value: ['off', 'on', 'auto'], required: true}
        }
      }
    },
    getBluetoothInfo: {
      name: 'Get Bluetooth Info',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    setBluetoothStandby: {
      name: 'Set Bluetooth Standby',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', required: true}
        }
      }
    },
    setBluetoothTxSetting: {
      name: 'Set Bluetooth Transmission',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', required: true}
        }
      }
    },
    getBluetoothDevicelist: {
      name: 'Get Bluetooth Device List',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    updateBluetoothDeviceList: {
      name: 'Update Bluetooth Device List',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system']
    },
    connectBluetoothDevice: {
      name: 'Connect Bluetooth Device',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          address: {type: 'string', value:'any', required:true}
        }
      }
    },
    disconnectBluetoothDevice: {
      name: 'Disconnect Bluetooth Device',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system']
    },
    getFuncStatus: {
      name: 'Get Func Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    setAutoPowerStandby: {
      name: 'Set Auto Power Standby Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', required: true}
        }
      }
    },
    setIrSensor: {
      name: 'Set Remote Control IR Sensor',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', required: true}
        }
      }
    },
    setSpeakerA: {
      name: 'Set Speaker A Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', required: true}
        }
      }
    },
    setSpeakerB: {
      name: 'Set Speaker B Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', value: [true, false], required: true}
        }
      }
    },
    setDimmer: {
      name: 'Set Speaker A Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          value: {type: 'integer', value: [-1, 0, 1, 2, 3, 4], required: true}
        }
      }
    },
    setZoneBVolumeSync: {
      name: 'Set Zone B Volume Sync with Zone A',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', value: [true, false], required: true}
        }
      }
    },
    setHdmiOut1: {
      name: 'Enable/Disbale HDMI OUT 1',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', value: [true, false], required: true}
        }
      }
    },
    setHdmiOut2: {
      name: 'Enable/Disbale HDMI OUT 2',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          enable: {type: 'boolean', value: [true, false], required: true}
        }
      }
    },
    getNameText: {
      name: 'Get Name Text',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    setNameText: {
      name: 'Set Text info for Zone & Input',
      apiVersion: 'v1',
      method: 'POST',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          id: {type: 'string', value:'any', required:true},
          text: {type: 'string', value:'any', required:true}
        }
      }
    },
    getLocationInfo: {
      name: 'Get Location Information',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['system']
    },
    sendIrCode: {
      name: 'Send Specific IR Code',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['system'],
      params: {
        system: {
          code: {type: 'string', value: 'any', required: true}
        }
      }
    },
    // Zone Commands
    getStatus: {
      name: 'Get Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['main', 'zone2', 'zone3', 'zone4']
    },
    getSoundProgramList: {
      name: 'Get Sound Progams',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          zone: {type:'string', value:['main', 'zone2', 'zone3', 'zone4'], required:true}
        },
        zone2: {
          zone: {type:'string', value:['main', 'zone2', 'zone3', 'zone4'], required:true}
        },
        zone3: {
          zone: {type:'string', value:['main', 'zone2', 'zone3', 'zone4'], required:true}
        },
        zone4: {
          zone: {type:'string', value:['main', 'zone2', 'zone3', 'zone4'], required:true}
        }
      }
    },
    setPower: {
      name: 'Set Power Status of a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          power: {type: 'string', value: ['on', 'standby', 'toggle'], required: true}          
        },
        zone2: {
          power: {type: 'string', value: ['on', 'standby', 'toggle'], required: true}          
        },
        zone3: {
          power: {type: 'string', value: ['on', 'standby', 'toggle'], required: true}          
        },
        zone4: {
          power: {type: 'string', value: ['on', 'standby', 'toggle'], required: true}          
        }
      }
    },
    setSleep: {
      name: 'Set Sleep Timer for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          sleep: {type: 'integer', value: [0, 30, 60, 90, 120], required: true}
        },
        zone2: {
          sleep: {type: 'integer', value: [0, 30, 60, 90, 120], required: true}
        },
        zone3: {
          sleep: {type: 'integer', value: [0, 30, 60, 90, 120], required: true}
        },
        zone4: {
          sleep: {type: 'integer', value: [0, 30, 60, 90, 120], required: true}
        }
      }
    },
    setVolume: {
      name: 'Set Volume for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          volume: {type: 'integer', value: 'any', required: true},
          step: {type: 'integer', value: 'any', required: true}
        },
        zone2: {
          volume: {type: 'integer', value: 'any', required: true},
          step: {type: 'integer', value: 'any', required: true}
        },
        zone3: {
          volume: {type: 'integer', value: 'any', required: true},
          step: {type: 'integer', value: 'any', required: true}
        },
        zone4: {
          volume: {type: 'integer', value: 'any', required: true},
          step: {type: 'integer', value: 'any', required: true}
        }
      }
    },
    setMute: {
      name: 'Set Mute Status for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          enable: {type: 'boolean', required: true}
        },
        zone2: {
          enable: {type: 'boolean', required: true}
        },
        zone3: {
          enable: {type: 'boolean', required: true}
        },
        zone4: {
          enable: {type: 'boolean', required: true}
        }
      }
    },
    setInput: {
      name: 'Select Zone Input',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          input: {type: 'string', value: 'any', required: true},
          mode: {type: 'string', value: 'autoplay_disabled', required: false}
        },
        zone2: {
          input: {type: 'string', value: 'any', required: true},
          mode: {type: 'string', value: 'autoplay_disabled', required: false}
        },
        zone3: {
          input: {type: 'string', value: 'any', required: true},
          mode: {type: 'string', value: 'autoplay_disabled', required: false}
        },
        zone4: {
          input: {type: 'string', value: 'any', required: true},
          mode: {type: 'string', value: 'autoplay_disabled', required: false}
        }
      }
    },
    setSoundProgram: {
      name: 'Set Sound Program',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          program: {type: 'string', value: 'any', required: true}
        },
        zone2: {
          program: {type: 'string', value: 'any', required: true}
        },
        zone3: {
          program: {type: 'string', value: 'any', required: true}
        },
        zone4: {
          program: {type: 'string', value: 'any', required: true}
        }
      }
    },
    set3dSurround: {
      name: 'Set 3D Surround for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          enable: {type: 'boolean', required: true}
        },
        zone2: {
          enable: {type: 'boolean', required: true}
        },
        zone3: {
          enable: {type: 'boolean', required: true}
        },
        zone4: {
          enable: {type: 'boolean', required: true}
        }
      }
    },
    setDirect: {
      name: 'Set Direct Status for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          enable: {type:'boolean', required:true}
        },
        zone2: {
          enable: {type:'boolean', required:true}
        },
        zone3: {
          enable: {type:'boolean', required:true}
        },
        zone4: {
          enable: {type:'boolean', required:true}
        }
      }
    },
    setPureDirect: {
      name: 'Set Pure Direct Status for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          enable: {type:'boolean', required:true}
        },
        zone2: {
          enable: {type:'boolean', required:true}
        },
        zone3: {
          enable: {type:'boolean', required:true}
        },
        zone4: {
          enable: {type:'boolean', required:true}
        }
      }
    },
    setEnhancer: {
      name: 'Set Enhancer Status for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          enable: {type:'boolean', required:true}
        },
        zone2: {
          enable: {type:'boolean', required:true}
        },
        zone3: {
          enable: {type:'boolean', required:true}
        },
        zone4: {
          enable: {type:'boolean', required:true}
        }
      }
    },
    setToneControl: {
      name: 'Set Tone Control for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          // TODO: Figure out what the values of 'mode' should be (page 42)
          mode: {type:'string', value: ['', 'manual'], required:false},
          bass: {type:'integer', required:false},
          treble: {type:'integer', required:false}
        },
        zone2: {
          mode: {type:'string', value: ['', 'manual'], required:false},
          bass: {type:'integer', required:false},
          treble: {type:'integer', required:false}
        },
        zone3: {
          mode: {type:'string', value: ['', 'manual'], required:false},
          bass: {type:'integer', required:false},
          treble: {type:'integer', required:false}
        },
        zone4: {
          mode: {type:'string', value: ['', 'manual'], required:false},
          bass: {type:'integer', required:false},
          treble: {type:'integer', required:false}
        }
      }
    },
    setEqualizer: {
      name: 'Set Equalizer for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          // TODO: Figure out what the values of 'mode' should be (page 43)
          mode: {type:'string', value: ['', 'maunal'], required:false},
          low: {type:'integer', required:false},
          high: {type:'integer', required:false},
          mid: {type:'integer', required:false}
        },
        zone2: {
          mode: {type:'string', value: ['', 'maunal'], required:false},
          low: {type:'integer', required:false},
          high: {type:'integer', required:false},
          mid: {type:'integer', required:false}
        },
        zone3: {
          mode: {type:'string', value: ['', 'maunal'], required:false},
          low: {type:'integer', required:false},
          high: {type:'integer', required:false},
          mid: {type:'integer', required:false}
        },
        zone4: {
          mode: {type:'string', value: ['', 'maunal'], required:false},
          low: {type:'integer', required:false},
          high: {type:'integer', required:false},
          mid: {type:'integer', required:false}
        }
      }
    },
    setBalance: {
      name: 'Set Left/Right Balanace for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          value: {type:'integer', required:true}
        },
        zone2: {
          value: {type:'integer', required:true}
        },
        zone3: {
          value: {type:'integer', required:true}
        },
        zone4: {
          value: {type:'integer', required:true}
        }
      }
    },
    setDialogueLevel: {
      name: 'Set Dialogue Level for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          value: {type:'integer', required:true}
        },
        zone2: {
          value: {type:'integer', required:true}
        },
        zone3: {
          value: {type:'integer', required:true}
        },
        zone4: {
          value: {type:'integer', required:true}
        }
      }
    },
    setDialogueLift: {
      name: 'Set Dialogue Lift for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          value: {type:'integer', required:true}
        },
        zone2: {
          value: {type:'integer', required:true}
        },
        zone3: {
          value: {type:'integer', required:true}
        },
        zone4: {
          value: {type:'integer', required:true}
        }
      }
    },
    setClearVoice: {
      name: 'Set Clear Voice for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          enable: {type:'boolean', required:true}
        },
        zone2: {
          enable: {type:'boolean', required:true}
        },
        zone3: {
          enable: {type:'boolean', required:true}
        },
        zone4: {
          enable: {type:'boolean', required:true}
        }
      }
    },
    setSubwooferVolume: {
      name: 'Set Subwoofer Volume for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          volume: {type:'integer', required:true}
        },
        zone2: {
          volume: {type:'integer', required:true}
        },
        zone3: {
          volume: {type:'integer', required:true}
        },
        zone4: {
          volume: {type:'integer', required:true}
        }
      }
    },
    setBassExtension: {
      name: 'Set Bass Extension for a Zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          enable: {type:'boolean', required:true}
        },
        zone2: {
          enable: {type:'boolean', required:true}
        },
        zone3: {
          enable: {type:'boolean', required:true}
        },
        zone4: {
          enable: {type:'boolean', required:true}
        }
      }
    },
    getSignalinfo: {
      name: 'Get Signal Info',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
    },
    prepareInputChange: {
      name: 'Let a Device do necessary process before changing input in a specific zone',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['main', 'zone2', 'zone3', 'zone4'],
      params: {
        main: {
          input: {type: 'string', value: 'any', required: true}
        },
        zone2: {
          input: {type: 'string', value: 'any', required: true}
        },
        zone3: {
          input: {type: 'string', value: 'any', required: true}
        },
        zone4: {
          input: {type: 'string', value: 'any', required: true}
        }
      }
    },
    // Tuner & Network/USB
    getPresetInfo: {
      name: 'Get Preset Info',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['tuner', 'netusb'],
      params: {
        tuner: {
          band: {type:'string', value:['common', 'am', 'fm', 'dab'], required:true}
        },
        netusb: {}
      }
    },
    getPlayInfo: {
      name: 'Get Playback Info',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['tuner', 'netusb', 'cd']
    },
    setBand: {
      name: 'Set Tuner Band',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner'],
      params: {
        tuner: {
          band: {type:'string', value:['dab', 'fm', 'am'], required:true}
        }
      }
    },
    setFreq: {
      name: 'Set Tuner Frequency',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner'],
      params: {
        tuner: {
          band: {type:'string', value:['fm', 'am', 'dab'], required:true},
          num: {type:'integer', required:true}
        }
      }
    },
    recallPreset: {
      name: 'Recall a Tuner Preset',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner', 'netusb'],
      params: {
        tuner: {
          band: {type:'string', value:['dab', 'fm', 'am'], required:true}          
        },
        netusb: {
          zone: {type:'string', value:['main','zone2','zone3','zone4'], required:true},
          num: {type:'integer', required:true}
        }
      }
    },
    switchPreset: {
      name: 'Switch Preset',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner'],
      params: {
        tuner: {
          dir: {type:'string', value:['next', 'previous'], required:true}
        }
      }
    },
    storePreset: {
      name: 'Store Tuner Preset',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner','netusb'],
      params: {
        tuner: {
          num: {type:'integer', required:true}          
        },
        netusb: {
          num: {type:'integer', required:true}          
        }
      }
    },
    clearPreset: {
      name: 'Clear Tuner Preset',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner','netusb'],
      params: {
        tuner: {
          band: {type:'string', value:['common', 'am', 'fm', 'dab'], required:true},
          num: {type:'integer', required:true}          
        },
        netusb: {
          num: {type:'integer', required:true}          
        }
      }
    },
    startAutoPreset: {
      name: 'Automatically set Tuner Presets',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner'],
      params: {
        tuner: {
          band: {type:'string', value:['fm'], required:true}
        }
      }
    },
    cancelAutoPreset: {
      name: 'Cancel the automatic set Tuner Presets process',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner'],
      params: {
        tuner: {
          band: {type:'string', value:['fm'], required:true}
        }
      }
    },
    movePreset: {
      name: 'Move Tuner Preset',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner','netusb'],
      params: {
        tuner: {
          band: {type:'string', value:['common', 'am', 'fm', 'dab'], required:true},
          from: {type:'integer', required:true},
          to: {type:'integer', required:true}          
        },
        netusb: {
          from: {type:'integer', required:true},
          to: {type:'integer', required:true}          
        }
      }
    },
    startDabInitialScan: {
      name: 'Start DAB Tuner Initial Scan',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner']
    },
    cancelDabInitialScan: {
      name: 'Cancel DAB Tuner Initial Scan',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner']
    },
    setDabTuneAid: {
      name: 'Execeute DAB Tuner Aid',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner'],
      params: {
        tuner: {
          action: {type:'string', value:['start', 'stop', 'up', 'down'], required:true}
        }
      }
    },
    setDabService: {
      name: 'Select DAB Tuner Service',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['tuner'],
      params: {
        tuner: {
          dir: {type:'string', value:['next', 'previous'], required:true}
        }
      }
    },
    // Network/USB
    setPlayBack: {
      name: 'Set Playback Status',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['netusb', 'cd'],
      params: {
        netusb: {
          playback: {type: 'string', value:['play', 'stop', 'pause', 'play_pause', 'previous', 'next', 'fast_reverse_start', 'fast_reverse_end', 'fast_forward_start', 'fast_forward_end'], required: true}
        },
        cd: {
          playback: {type: 'string', value:['play', 'stop', 'pause', 'previous', 'next', 'fast_reverse_start', 'fast_reverse_end', 'fast_forward_start', 'fast_forward_end', 'track_select'], required: true},
          num: {type:'integer', required:false}
        }
      }
    },
    setPlayPosition: {
      name: 'Set Track Play Position',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['netusb'],
      params: {
        netusb: {
          position: {type: 'integer', required: true}
        }
      }
    },
    toggleRepeat: {
      name: 'Toggle the Repeat Setting',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['netusb','cd']
    },
    toggleShuffle: {
      name: 'Toggle the Shuffle Setting',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['netusb','cd']
    },
    toggleTray: {
      name: 'Open/Close the CD Tray',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['cd']
    },
    getListInfo: {
      name: 'Retrieve List Information',
      apiVersion: 'v1',
      method: 'GET',
      type: 'status',
      controllers: ['netusb'],
      params: {
        netusb: {
          list_id: {type:'string',value:['main','auto_complete','search_artist','serach_track'],required:false},
          input: {type:'string',required:true},
          index: {type:'integer',required:false},
          size: {type:'integer',required:true},
          lang: {type:'string',value:['en','ja','fr','de','es','ru','it','zh'],required:false}
        }
      }
    },
    setListControl: {
      name: 'Control List Information',
      apiVersion: 'v1',
      method: 'GET',
      type: 'control',
      controllers: ['netusb'],
      params: {
        netusb: {
          list_id: {type:'string',value:['main','auto_complete','search_artist','serach_track'],required:false},
          type: {type:'string',required:true},
          index: {type:'integer',required:false},
          zone: {type:'string',value:['main','zone2','zone3','zone4'],required:false}
        }
      }
    }
  },
  setSearchString: {
    name: 'Set Search Text',
    apiVersion: 'v1',
    method: 'POST',
    type: 'control',
    controllers: ['netusb'],
    params: {
      netusb: {
        list_id: {type:'string',value:['main','auto_complete','search_artist','serach_track'],required:false},
        string: {type:'string',required:true},
        index: {type:'integer',required:false}
      }
    }
  },
  getSettings: {
    name: 'Get Setup of Net/USB',
    apiVersion: 'v1',
    method: 'GET',
    type: 'status',
    controllers: ['netusb','clock']
  },
  setQuality: {
    name: 'Set Streaming Quality',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['netusb'],
    params: {
      netusb: {
        input: {type:'string',required:true},
        value: {type:'string',value:['hr_192_24','hr_96_24','cd_44_16','mp3_320'], required:true}
      }
    }
  },
  getRecentInfo: {
    name: 'Retrieve Playback History',
    apiVersion: 'v1',
    method: 'GET',
    type: 'status',
    controllers: ['netusb']
  },
  recallRecentInfo: {
    name: 'Get Setup of Net/USB',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['netusb'],
    params: {
      netusb: {
        zone: {type:'string', value:['main','zone2','zone3','zone4'], required:true},
        num: {type:'integer', required:true}
      }
    }
  },
  clearRecentInfo: {
    name: 'Clear Recent History Information',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['netusb']
  },
  managePlay: {
    name: 'Special Processing for Track',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['netusb'],
    params: {
      netusb: {
        type: {type:'string', value:['add_bookmark','add_track','add_album','add_channel_track','add_playlist','thumbs_up','thumbs_down','mark_tired'], required:true},
        bank: {type:'integer', required:false},
        timeout: {type:'integer', required:true}
      }
    }
  },
  manageList: {
    name: 'Manage the Net/USB List',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['netusb'],
    params: {
      netusb: {
        list_id: {type:'string',value:['main','auto_complete','search_artist','serach_track'],required:false},
        type: {type:'string', value:['add_bookmark','add_track','add_album','add_channel','add_playlist','remove_bookmark','remove_track','remove_album','remove_artist','remove_channel','remove_playlist','end_auto_complete'], required:true},
        index: {type:'integer',required:false},
        zone: {type:'string', value:['main','zone2','zone3','zone4'], required:false},
        bank: {type:'integer', required:false},
        timeout: {type:'integer', required:true}
      }
    }
  },
  getPlayDescription: {
    name: 'Retrieve Detailed Track Information',
    apiVersion: 'v1',
    method: 'GET',
    type: 'status',
    controllers: ['netusb'],
    params: {
      netusb: {
        type: {type:'string', value:['why_this_song'], required:true},
        timeout: {type:'integer', required:true}
      }
    }
  },
  setListSortOption: {
    name: 'Get Setup of Net/USB',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['netusb'],
    params: {
      netusb: {
        input: {type:'string', value:['pandora'], required:true},
        type: {type:'string', value:['date','alphabet'], required:true}
      }
    }
  },
  getAccountStatus: {
    name: 'Get Account Status',
    apiVersion: 'v1',
    method: 'GET',
    type: 'status',
    controllers: ['netusb']
  },
  switchAccount: {
    name: 'Switch Account for Service',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['netusb'],
    params: {
      netusb: {
        input: {type:'string', value:['pandora'], required:true},
        index: {type:'integer',required:true},
        timeout: {type:'integer', required:true}
      }
    }
  },
  getServiceInfo: {
    name: 'Get Streaming Service Information',
    apiVersion: 'v1',
    method: 'GET',
    type: 'status',
    controllers: ['netusb'],
    params: {
      netusb: {
        input: {type:'string', value:['pandora'], required:true},
        type: {type:'string', value:['account_list','licensing','activation_code'], required:true},
        timeout: {type:'integer', required:true}
      }
    }
  },
  // Clock
  setAutoSync: {
    name: 'Set Clock Sync Settings',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['clock'],
    params: {
      clock: {
        enable: {type:'boolean', required:true}
      }
    }
  },
  setDateAndTime: {
    name: 'Set the Date & Time',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['clock'],
    params: {
      clock: {
        date_time: {type:'string',value:['YYMMDDhhmmss'], required:true}
      }
    }
  },
  setClockFormat: {
    name: 'Get Setup of Net/USB',
    apiVersion: 'v1',
    method: 'GET',
    type: 'control',
    controllers: ['clock'],
    params: {
      clock: {
        format: {type:'string', values:['12h','24h'], required:true}
      }
    }
  },
  setAlarmSettings: {
    name: 'Get Setup of Net/USB',
    apiVersion: 'v1',
    method: 'POST',
    type: 'control',
    controllers: ['clock'],
    params: {
      clock: {
        alarm_on: {type:'boolean', required:false},
        volume: {type:'integer', required:false},
        fade_interval: {type:'integer', required:false},
        fade_type: {type:'integer', required:false},
        mode: {type:'string', required:false},
        repeat: {type:'boolean', required:false},
        detail: {type:'object', required:false},
        day: {type:'string', values:['sunday','sunday','monday','tuesday','wednesday','thursday','friday','saturday'], required:false},
        enable: {type:'boolean', required:false},
        time: {type:'string', values:['hhmm'], required:false},
        beep: {type:'boolean',  required:false},
        playback_type: {type:'string', values:['resume','preset'], required:false},
        resume: {type:'object', required:false},
        input: {type:'string', required:false},
        preset: {type:'object', required:false},
        type: {type:'string', required:false},
        num: {type:'integer', required:false}
      }
    }
  }
};
